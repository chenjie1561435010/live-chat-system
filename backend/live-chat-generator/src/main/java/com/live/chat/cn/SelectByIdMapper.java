package com.live.chat.cn;

/**
 * @Author Chen Jie
 * @Date 2021/10/10 9:50
 * @Version 1.0
 */
public interface SelectByIdMapper {
    <T> T selectById(Integer id);
}
